"use strict";

const java = require('java');
java.classpath.push(__dirname + "/java/alto.score-0.2.1-standalone.jar");
const AnomalyDetector = java.import('alto.score');

function distributionAnomaly (subject, context) {
  let [score, confidence] = AnomalyDetector.scoreSync(subject, context).tail;

  // Let's ensure we're properly constraining the score and confidence.  In
  // cases of the score being out of bounds, confidence should drop to 0.
  // This is enforced here, but should also be enforced in the core module.
  
  if (isNaN(score)) {
    score = 0;
    confidence = 0;
  }
  else if (score < 0) {
    score = 0;
    confidence = 0;
  }
  // In this case, we keep whatever confidence we had.
  else if (score > 10) {
    score = 10;
  }

  return [Math.abs(score), Math.abs(confidence)];
}

module.exports = distributionAnomaly;

