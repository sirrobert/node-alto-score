"use strict";

const calcScore = require("./index.js");

var iter    = 250;
var maxRows = 200;
var maxCols = 1000;

console.log(`
Distribution Anomaly Score Demo

private.node-alto-score

The following demonstration measures anomalies in the JavaScript engine's
Math.random() function.

In the output below, "rows" refers to the number of distribution groups in
the context.  The "cols" field represents the number of values per
observation distribution (in both the subject and context groups).

For each rows/cols combination, the reported score and confidence are the mean score and confidence over ${iter} iterations.

The process will end after ${maxRows} rows x ${maxCols} cols.

At the end of the process, the average time per calculation is displayed.
`);

var start = new Date();
var totalIterations = 0;

console.log("rows\tcols\tscore\tconfidence\tmean ms-per-calc");
for (var r = 50; r <= 200; r += 50) {
  for (var c = 100; c <= 1000; c += 100) {
    var metascore = 0;
    var metaconf  = 0;

    let localStart = new Date();
    for (var i = 0; i < iter; i++) {
      totalIterations++;
      let [score, confidence] = calcScore(getSubject(c), getContext(r,c));
      metascore += score;
      metaconf  += confidence;
      process.stdout.write(i + "\r");
    }
    let localEnd  = new Date();
    let localDiff = localEnd - localStart;
    let localPer  = localDiff / iter;

    process.stdout.write("    \r");
    console.log(
      r
      + "\t" +
      c
      + "\t" +
      parseFloat((metascore/iter).toFixed(3))
      + "\t" +
      parseFloat((metaconf/iter).toFixed(8))
      + "\t" +
      localPer
    );
  }
}

function getValues (cols) {
  let arr = [];
  for (var i = 0; i < cols; i++) {
    arr.push(Math.random());
  }
  return arr;
}

function getSubject (cols) {
  return getValues(cols);
}

function getContext (rows, cols) {
  var context = [];
  for (var i = 0; i < rows; i++) {
    context.push(getValues(cols));
  }
  return context;
}

var end = new Date();

var diff = end - start;
var per = diff / totalIterations;
console.log("Mean milliseconds per calculation:", per);
