# private.node-alto-score

Altometrics's scoring module hosted in JavaScript (via Node JS).

# Algorithm Information

See the documentation in the Clojure repository (alto.score) for more
information about the technical aspects of the algorithm.

# Usage

Scores a single array of values (the *subject*) against a set of arrays of
values (collectively the *context*).  

```javascript
const calcScore = require('private.node-alto-score');

var subject = getSubjectData();
var context = getContextData();

let [score, confidence] = calcScore(subject, context);
```

# Interpretation of the Methodology

The scoring module is a method of identifying anomalies in data.  In
general, real-life processes are monitored.  Each record of the process is a
single *observation*.

Typically, anomaly detection processes focus on identifying observations
that do not fit well with either:

* another set of observations, or
* a canonical definition of "order" (defined externally)

## Methodological Distinction

Many other anomaly detection methods focus on identifying when a single
*observation* is anomalous.  Unlike those methods, the approach offered by
this library is different.  Rather than measure individual observations
against a group of observations,  *groups* of observations (sometimes called
*distributions* of observations) are measured against collections of other
*groups* (collections of observation *distributions*).

### An Example

Let's take a practical example.

#### Raw Data

For example, consider the following time-series of records recorded every
few seconds:

```
Wed Aug 09 2017 16:56:28 2.34
Wed Aug 09 2017 16:56:29 4.32
Wed Aug 09 2017 16:56:32 9.81
Wed Aug 09 2017 16:56:38 2.25
Wed Aug 09 2017 16:56:45 5.35
Wed Aug 09 2017 16:56:52 0.05
Wed Aug 09 2017 16:56:56 8.03
Wed Aug 09 2017 16:57:02 5.24
Wed Aug 09 2017 16:57:04 2.97
Wed Aug 09 2017 16:57:04 9.76
Wed Aug 09 2017 16:57:08 7.12
Wed Aug 09 2017 16:57:12 4.72
Wed Aug 09 2017 16:57:16 9.16
Wed Aug 09 2017 16:57:25 4.27
Wed Aug 09 2017 16:57:31 2.45
Wed Aug 09 2017 16:57:36 9.11
Wed Aug 09 2017 16:57:44 6.13
Wed Aug 09 2017 16:57:49 6.01
Wed Aug 09 2017 16:57:53 3.86
Wed Aug 09 2017 16:58:01 7.97
Wed Aug 09 2017 16:58:05 9.44
Wed Aug 09 2017 16:58:11 6.22
Wed Aug 09 2017 16:58:17 7.16
Wed Aug 09 2017 16:58:23 4.23
Wed Aug 09 2017 16:58:24 5.18
Wed Aug 09 2017 16:58:25 8.24
Wed Aug 09 2017 16:58:35 3.29
Wed Aug 09 2017 16:58:45 9.83
Wed Aug 09 2017 16:58:45 2.93
Wed Aug 09 2017 16:58:47 5.43
...
```

#### A Typical Approach

Many algorithms will attempt to detect, say, that sixth observation 
(`Wed Aug 09 2017 16:56:52 0.05`) is anomalously low compared with the rest
of the data.

#### This Module's Approach

The algorithm provided by this module, however does not attempt to determine
anomalousness of individual observations.  Rather, a *distribution* is
measured against *a group of other distributions*.

#### Creating a *Subject* and *Context*

An example of such groupings in this data may look like this:

```
Context Group 1
Wed Aug 09 2017 16:56:28 2.34
Wed Aug 09 2017 16:56:29 4.32
Wed Aug 09 2017 16:56:32 9.81
Wed Aug 09 2017 16:56:38 2.25
Wed Aug 09 2017 16:56:45 5.35

Context Group 2
Wed Aug 09 2017 16:56:52 0.05
Wed Aug 09 2017 16:56:56 8.03
Wed Aug 09 2017 16:57:02 5.24
Wed Aug 09 2017 16:57:04 2.97
Wed Aug 09 2017 16:57:04 9.76

Context Group 2
Wed Aug 09 2017 16:57:08 7.12
Wed Aug 09 2017 16:57:12 4.72
Wed Aug 09 2017 16:57:16 9.16
Wed Aug 09 2017 16:57:25 4.27
Wed Aug 09 2017 16:57:31 2.45

Context Group 2
Wed Aug 09 2017 16:57:36 9.11
Wed Aug 09 2017 16:57:44 6.13
Wed Aug 09 2017 16:57:49 6.01
Wed Aug 09 2017 16:57:53 3.86
Wed Aug 09 2017 16:58:01 7.97

Context Group 2
Wed Aug 09 2017 16:58:05 9.44
Wed Aug 09 2017 16:58:11 6.22
Wed Aug 09 2017 16:58:17 7.16
Wed Aug 09 2017 16:58:23 4.23
Wed Aug 09 2017 16:58:24 5.18

Subject
Wed Aug 09 2017 16:58:25 8.24
Wed Aug 09 2017 16:58:35 3.29
Wed Aug 09 2017 16:58:45 9.83
Wed Aug 09 2017 16:58:45 2.93
Wed Aug 09 2017 16:58:47 5.43
```

Each group has about 30 seconds worth of data.  Because the most recent
timestamps are at the bottom of this list (the subject), a "common-sense"
reading of this would be something like, "How anomalous is *this* 30 seconds
worth of data compared with 30-second distributions *historically*?"

#### Converting to JSON

By ignoring the timestamps, we can convert these into JSON data structures,
like so:

```
let context = [
  [2.34, 4.32, 9.81, 2.25, 5.35],
  [0.05, 8.03, 5.24, 2.97, 9.76],
  [7.12, 4.72, 9.16, 4.27, 2.45],
  [9.11, 6.13, 6.01, 3.86, 7.97],
  [9.44, 6.22, 7.16, 4.23, 5.18]
]

let subject = [8.24, 3.29, 9.83, 2.93, 5.43];
```

This subject would then be scored against the context and we would get a
result like this:

    // score     confidence
    [3.42378928  2.14890922]

On the other hand, we could switch the process and take context group 2 as
the subject and put the current subject into the context.  Then we would
have:

```
let context = [
  [2.34, 4.32, 9.81, 2.25, 5.35],
  // This line was removed, to become the subject
  // [0.05, 8.03, 5.24, 2.97, 9.76],
  [7.12, 4.72, 9.16, 4.27, 2.45],
  [9.11, 6.13, 6.01, 3.86, 7.97],
  [9.44, 6.22, 7.16, 4.23, 5.18],
  // This used to be the subject.  It is now context.
  [8.24, 3.29, 9.83, 2.93, 5.43]
]

let subject = [0.05, 8.03, 5.24, 2.97, 9.76];
```

Scoring this new arrangement of data, we would get a higher anomaly score
(but the same confidence):

    // score     confidence
    [4.19284358  2.14890922]

The anomaly for the overall distribution is higher because it contains the
single low value; though in doing this we have not identified a *specific*
anomalous data point, but an anomalous distribution of data.

There are a variety of procedures that can be undertaken to utilize this
anomaly measurement to:

* Identify anomalous individual anomalous observations.
* Identify previously-unknown "normal" patterns
* Classify real-world events of unknown characteristic
* ... and much more.

It is left as an exercise for the reader to implement such innovations in
other libraries that make use of this one.

# Interpretation of Results

The anomaly scoring returns two values:  a measurement of the anomalousness
of the subject against the context and a measurement of the confidence in
the first measurement.  We'll refer to these simply as the *score* and the
*confidence*, respectively.

## Interpreting the Score

Scores can range from 0 - 10.  The scale is logarithmic (base 10), meaning
that each integer increase in score represents a ten-fold increase in
severity of anomaly.  

Generally, there is an intuitive meaning of scores, illustrated in this chart:

<table>
  <thead>
    <tr>
      <th>Score</th>
      <th>Intuitive Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr style="background-color: #000000; color: #ffffff;">
      <td>0</td>
      <td>Extremely low anomaly; possible data error</td>
    </tr>
    <tr style='background-color: #66bb5c;'>
      <td>1</td>
      <td rowspan=2>low anomaly; normal noise in natural processes</td>
    </tr>
    <tr style='background-color: #66bb5c;'>
      <td>2</td>
    </tr>

    <tr style='background-color: #d1eb6e;'>
      <td>3</td>
      <td rowspan=3>moderate anomaly; unusual data events occuring</td>
    </tr>
    <tr style='background-color: #d1eb6e;'>
      <td>4</td>
    </tr>
    <tr style='background-color: #d1eb6e;'>
      <td>5</td>
    </tr>

    <tr style='background-color: #db813a;'>
      <td>6</td>
      <td rowspan=3>severe anomaly; major data disruption</td>
    </tr>
    <tr style='background-color: #db813a;'>
      <td>7</td>
    </tr>
    <tr style='background-color: #db813a;'>
      <td>8</td>
    </tr>

    <tr style='background-color: #d90836; color: white;'>
      <td>9</td>
      <td rowspan=2>extremly high anomaly; possible data error</td>
    </tr>
    <tr style='background-color: #d90836; color: white'>
      <td>10</td>
    </tr>
  </tbody>
  <tfoot>
  </tfoot>
</table>



### A Score of 0

A score of 0 means that there's precisely *no* variation in the
subject data compared with the context data.  This is extremely uncommon and
*may* be an indicator of a data error.  An example of data that will produce
a zero score follows.

```javascript
let subject = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
let context = [
  [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
  [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
  [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
  ...
  [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1]
];

let [score, confidence] = calcAnomaly(subject, context);

// Score      0
// Confidence 0
```

This can be intuited without a fancy algorithm:  there's 0 anomaly in this
data.  The confidence score of 0 is discussed in the section *Interpreting
the Confidence*.

### A Score of 10

A score of 10 represents an extremely large anomaly (the highest possible).
Since scoring happens on a log<sub>10</sub> scale, an anomaly score of 10 is
1,000,000,000 times larger than a score of 1.  In practice this kind of
result is virtually impossible to get with (meaningful) real-world data and is,
therefore, defined as the maximum value.  An unscaled anomaly measurement of
10,000,000,001 would produce a score of 10.00000000004343, which is
truncated to 10.  Technically, an unscaled anomaly measurement can be
arbitrarily high--though, as stated, is effectively unreachable in
meaingful, real-world data.

## Interpreting the Confidence

In plain terms, confidence is a measure of how well-grounded the score is,
statistically.  More technically, it is a measure of the informational
entropy in the of the subject and context data.  This means that, for
example, an anomaly in data with very little variation has a low confidence,
while an anomaly in data with a good distribution of values (in the subject
and context) has a high confidence.

The meaning of the confidence scale is reversed from the score:  a higher
confidence is a good thing.

Like the score, the lower bound on confidence is 0.  Unlike the score,
however, there is no upper-bound on the confidence.  In practical terms,
though, a confidence of 6 is "pretty good" and a confidence of 9 or more is
"very confident".

<table style="width: 50%">
  <thead>
    <tr>
      <th>Score</th>
      <th>Intuitive Meaning</th>
    </tr>
  </thead>
  <tbody>
    <tr style='background-color: #d90836; color: white;'>
      <td>0</td>
      <td>Extremely low confidence; possible data error such as a low-data
      condition</td>
    </tr>

    <tr style='background-color: #db813a;'>
      <td>1</td>
      <td rowspan=2>low confidence; may be improved with more data</td>
    </tr>
    <tr style='background-color: #db813a;'>
      <td>1.5</td>
    </tr>

    <tr style='background-color: #d1eb6e;'>
      <td>2</td>
      <td rowspan=3>moderate confidence; unusual data events occuring</td>
    </tr>
    <tr style='background-color: #d1eb6e;'>
      <td>3</td>
    </tr>
    <tr style='background-color: #d1eb6e;'>
      <td>4</td>
    </tr>

    <tr style='background-color: #66bb5c;'>
      <td>5</td>
      <td rowspan=4>high confidence; data appears to well represent
      real-world information</td>
    </tr>
    <tr style='background-color: #66bb5c;'> <td>6</td> </tr>
    <tr style='background-color: #66bb5c;'> <td>7</td> </tr>
    <tr style='background-color: #66bb5c;'> <td>8</td> </tr>

    <tr style='background-color: #1d960f; color: white;'>
      <td>9</td>
      <td rowspan=2>extremely high confidence; there is plenty of data and
      it has excellent variation, indicative of good natural processes</td>
    </tr>
    <tr style='background-color: #1d960f; color: white;'> <td>10+</td> </tr>
  </tbody>
  <tfoot>
  </tfoot>
</table>

Here's an example of low-variation data that produces a low-confidence with
a score of 10.

```javascript
> let subject = [1,1,1,1,1,1,1,1,1,1,1,1,1,1,1];
> let context = [
  [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5],
  [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5],
  ...
  [5,5,5,5,5,5,5,5,5,5,5,5,5,5,5]
];

> console.log("Anomaly [score, confidence]:", score(subject,context));
Anomaly [score, confidence]: [10, 0]
```

In the example above, there is extremely low informational entropy (or,
equivalently, high redundancy).  The score is 10 because there's absolutely
nothing in the context that would predict the values in the subject.
However, because there is so little variation in the data, the data "feels"
unnatural.  This means that the confidence in the score is quite low.

On the other hand, we can have a very low anomaly measurement with high
confidence.  Consider the following example.  In this code, we measure the
overall anomaly of Node JS's `Math.random()` function.  We do this by
creating a subject entirely populated by random values, then a context
entirely populated by random values.

First, we create a function that generates an array of random values.

```javascript
function getValues () {
  let arr = [];
  for (var i = 0; i < 30; i++) {
    arr.push(Math.random());
  }
  return arr;
}
```

Next, we create functions to provide a subject and context.

```javascript
function getSubject () {
  return getValues();
}

function getContext () {
  var context = [];
  for (var i = 0; i < 30; i++) {
    context.push(getValues());
  }
  return context;
}
```

If we use these to compute the score and confidence:

    distAnomaly(getSubject(), getContext());

We get results like 

    [1.9589237459872949, 4.738947520394584]

If we average the score and confidence over 10,000 runs, we get results like
this:

    // score             confidence
    [2.3298347902348727, 4.929384729583712]

### Increasing Confidence 

As stated before, confidence has to do with both the volume and variation of
the data.

We didn't use 30 context groups (in `getContext()`) or 30 values per array
(in `getValues()`) for any particular reason, it was just a moderate value.

If we do the same thing but instead of 30 context groups and 30 values per
array, we use 100 groups and 800 values per array, we get results like this:

    // score             confidence
    [2.6247712420386695, 9.643856189774679]

Notice that the score stays about the same, but the confidence increases
dramatically.  Intuitively, this is because there's about the same
randomness (that accounts for the sameness of the score), but there is a lot
more data with, presumably, very few repeat values--meaning a higher
informational entropy and a higher confidence.

# Included Demo

```
Distribution Anomaly Score Demo

private.node-alto-score

The following demonstration measures anomalies in the JavaScript engine's
Math.random() function.

In the output below, "rows" refers to the number of distribution groups in the
context.  The "cols" field represents the number of values per observation
distribution (in both the subject and context groups).  

For each rows/cols combination, the reported score and confidence are the mean
score and confidence over 250 iterations.

The process will end after 200 rows x 1000 cols.

At the end of the process, the average time per calculation is displayed.

rows  cols    score    confidence      mean ms-per-calc
50    100     2.64     6.64385619      35.628
50    200     2.569    7.64385619      51.872
50    300     2.466    8.22881869      65.684
50    400     2.402    8.64385619      87.808
50    500     2.367    8.96578428      114.996
50    600     2.339    9.22881869      147.52
50    700     2.32     9.45121111      189.812
50    800     2.302    9.64385619      239.004
50    900     2.291    9.81378119      355.148
50    1000    2.272    9.96578428      413.896
100   100     2.8      6.64385619      38.04
100   200     2.898    7.64385619      63.772
100   300     2.796    8.22881869      113.936
100   400     2.742    8.64385619      178.88
100   500     2.716    8.96578428      256.452
100   600     2.686    9.22881869      295.772
100   700     2.646    9.45121111      396.26
100   800     2.625    9.64385619      494.196
100   900     2.606    9.81378119      629.348
100   1000    2.628    9.96578428      807.692
150   100     2.462    6.64385619      70.588
150   200     3.117    7.64385619      130.5
150   300     3.071    8.22881869      174.068
150   400     2.985    8.64385619      255.888
150   500     2.929    8.96578428      326.6
150   600     2.89     9.22881869      434.816
150   700     2.862    9.45121111      515.34
150   800     2.835    9.64385619      603.584
150   900     2.831    9.81378119      739.824
150   1000    2.809    9.96578428      867.452
200   100     2.29     6.64385619      68.696
200   200     3.074    7.64385619      143.996
200   300     3.234    8.22881869      225.38
200   400     3.181    8.64385619      317.372
200   500     3.123    8.96578428      420.048
200   600     3.076    9.22881869      533.536
200   700     3.043    9.45121111      673.108
200   800     3.011    9.64385619      845.088
200   900     2.974    9.81378119      1002.808
200   1000    2.965    9.96578428      1173.032

Mean milliseconds per calculation: 362.4385
```

## Performance

A chart of the `mean ms-per-calc` metric shows how quickly the algorithm
performed on the test machine (an Ubuntu 16.10 x64 virtual machine with 8GB RAM
and 3 cores).  The X-axis (formatted "50x100", for example) indicates the RowsxColumns in that test.

![Chart of mean milliseconds per calculation](./docs/mean-ms-per-calc.png)

In this chart, you can see that the major independent variable in speed is the number of observations per distribution.

The shape of the chart will be fairly consistent regardless of machine power,
but absolute speed should vary depending on the power of the processor.

# Building a Normal Profile
***This documentation is incomplete and needs work.***

The normal profile is a definition of normality, using the context data as
the normal-defining set, that can be applied to other data not in the context
to test how 'normal' it is. 

The profile uses "subspace method".  The idea of the subspace method is to  
re-describe the input data (i.e. the context data) using not the given
dimensions, but a different set of dimensions that are ordered such that the 
first dimension is in the direction of greatest variation of data, the second
(orthogonal) dimension is in the direction of greatest remaining variation, and 
so on down the line.  This is called a "principal components analysis", or PCA, 
because it finds the "principal components" (i.e. directions of greatest 
variation) in the data.  With real-world data, this usually results in a few 
dimensions that explain the bulk of the total variation, and the remainder 
could be considered "fine-tuning" to explain the remaining variation.  The 
subspace method separates these dimensions into the "normal" subspace and the 
residual or "error" subspace.  We "fold" the error subspace into a single
dimension by computing the "projection error" of each point, which is the 
distance between the point and its projection into the normal subspace.  Thus,
with `k` dimensions in the normal subspace, we effectively use a `k+1`
dimension profile for the data.  It is important that the error dimension not 
be too small; otherwise, the profile is overly specific to the given contextual
data, and will probably consider anything even slightly different from the 
context as an extremely different type of data.

The subspace method works by considering the contextual data as a matrix,
where each observation is a row of multivariate data; that is, rows are 
observations, and columns are dimensions.  (Yes, this does imply that all 
observations have the same number of points; this is what quantile vectors
from alto.score.qvec solve.)  Then, the matrix is "centered" such that the 
mean of each column is zero.  The original row of column means, called the 
"centroid" is saved for later.  Then, the centered matrix is decomposed by
the singular value decomposition (SVD) method.  This method decomposes the 
matrix into a product of three matrices, and it produces two items of
interest to our analysis: the list of singular values and the list of
dimensions.  The dimensions are the same as the principal components
mentioned before, and the singular values are essentially the standard
deviations along each of those dimensions in turn.  The lists produced by SVD 
are not necessarily sorted by importance, so that must be done manually.

Recall that the goal of a profile is to provide all the information necessary
to determine how well another observation, called the "subject", matches the 
observations in the context.  The details of that determination will be
described later; for now, we note that a profile must contain at least the 
following information:

1. the "centroid",
2. the list of singular values for the normal subspace,
3. the list of principal components for the normal subspace, and 
4. the list of projection errors for the contextual observations.

